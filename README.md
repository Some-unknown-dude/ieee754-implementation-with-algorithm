## Task description ##

Implement the [GetIEEE754Format](IEEE754FormatTask/DoubleExtension.cs#L13) method that obtains a binary representation of a real double-precision number in [IEEE 754 format](https://en.wikipedia.org/wiki/IEEE_754) as the extension method. **Don't use Framework's converter classes.**  *The task definition is given in the  XML-comments for this method.*   
*Hint:  Use C# structs [to create a union type](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/attributes/how-to-create-a-c-cpp-union-by-using-attributes) (similar to C unions).*

*Topics - bit operations, structs, extension methods, strings.*