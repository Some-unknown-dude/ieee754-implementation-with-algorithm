﻿using System;
using System.Net.NetworkInformation;

namespace IEEE754FormatTask
{
    public static class DoubleExtension
    {
        /// <summary>
        /// Returns a string representation of a double type number
        /// in the IEEE754 (see https://en.wikipedia.org/wiki/IEEE_754) format.
        /// </summary>
        /// <param name="number">Input number.</param>
        /// <returns>A string representation of a double type number in the IEEE754 format.</returns>
        public static string GetIEEE754Format(this double number)
        {
            if (double.IsNegativeInfinity(number))
            {
                return new string(ReturnInf(false));
            }

            if (double.IsPositiveInfinity(number))
            {
                return new string(ReturnInf(true));
            }

            if (double.IsNaN(number))
            {
                return new string(ReturnNaN());
            }

            const int doubleLength = 64, signPosition = 0, mantissaPositionFirst = 12;
            char[] binary = ReturnArrayWithZeros();
            binary[signPosition] = number < 0 ? '1' : '0';
            double absNumber = Math.Abs(number);
            int power = FindPowerOfTwo(absNumber);
            int offset = (power >= -1023) ? 0 : Math.Abs(power + 1023);
            if (offset > 51)
            {
                if (double.IsNegative(number))
                {
                    return new string(ReturnZero(false));
                }

                return new string(ReturnZero(true));
            }

            if (offset == 0)
            {
                PutBinaryNumberInArray(binary, mantissaPositionFirst - 1, power + 1023);
            }

            double powerOfTwo = Math.Pow(2, power);
            absNumber -= powerOfTwo;
            powerOfTwo /= 2;

            for (int i = mantissaPositionFirst + offset; i < doubleLength; ++i)
            {
                if (absNumber >= powerOfTwo)
                {
                    binary[i] = '1';
                    absNumber -= powerOfTwo;
                }

                powerOfTwo /= 2;
            }

            return new string(binary);
        }

        private static char[] ReturnArrayWithZeros()
        {
            int doubleLength = 64;
            char[] binary = new char[doubleLength];
            for (int i = 0; i < doubleLength; ++i)
            {
                binary[i] = '0';
            }

            return binary;
        }

        private static char[] ReturnInf(bool positive)
        {
            char[] binary = ReturnArrayWithZeros();
            int mantissaPosFirst = 12;
            if (!positive)
            {
                binary[0] = '1';
            }

            for (int i = 1; i < mantissaPosFirst; ++i)
            {
                binary[i] = '1';
            }

            return binary;
        }

        private static char[] ReturnNaN()
        {
            int mantissaPosFirst = 12;
            char[] binary = ReturnInf(false);
            binary[mantissaPosFirst] = '1';
            return binary;
        }

        private static char[] ReturnZero(bool positive)
        {
            char[] binary = ReturnArrayWithZeros();
            if (!positive)
            {
                binary[0] = '1';
            }

            return binary;
        }

        private static void PutBinaryNumberInArray(char[] array, int lastIndex, int number)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (lastIndex < 0 || lastIndex >= array.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(lastIndex));
            }

            if (lastIndex + 1 < number.GetBinaryLength())
            {
                throw new ArgumentOutOfRangeException(nameof(lastIndex), "Index is too close to the beginning");
            }

            for (int i = lastIndex; number != 0; --i)
            {
                array[i] = ((number & 1) == 0) ? '0' : '1';
                number /= 2;
            }
        }

        private static int GetBinaryLength(this int number)
        {
            if (number < 0)
            {
                throw new ArgumentException("Method works with positive numbers");
            }

            int powOfTwo = 1, length = 1;
            for (; powOfTwo < number; ++length)
            {
                powOfTwo *= 2;
            }

            return length;
        }

        /// <summary>
        /// Find the biggest power of two so that  2^powerOfTwo < <param name="number"/>.
        /// </summary>
        /// <param name="number">Number.</param>
        /// <returns>Poewr of two.</returns>
        private static int FindPowerOfTwo(double number)
        {
            if (number < 1)
            {
                return FindPowerOfTwoLessThanOne(number);
            }
            else if (number > 1)
            {
                return FindPowerOfTwoMoreThanOne(number);
            }

            return 0;
        }

        private static int FindPowerOfTwoLessThanOne(double number)
        {
            double powerOfTwo = 1;
            int power = 0;
            while (powerOfTwo > number)
            {
                powerOfTwo /= 2;
                --power;
            }

            return power;
        }

        private static int FindPowerOfTwoMoreThanOne(double number)
        {
            double powerOfTwo = 1;
            int power = 0;
            while (powerOfTwo <= number)
            {
                powerOfTwo *= 2;
                ++power;
            }

            return power - 1;
        }
    }
}
